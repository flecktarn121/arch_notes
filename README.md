# arch_notes

Small repo for notes on Arch GNU/Linux installation and maintenance. In short, is just s very big reminder for myself in case I have to do this again.

The whole thing is written in [asciidoc](https://asciidoctor.org).

It can be consulted [here](https://flecktarn121.gitlab.io/arch_notes).
